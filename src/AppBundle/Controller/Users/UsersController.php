<?php
/**
 * Created by PhpStorm.
 * User: Wandy
 * Date: 2018-03-07
 * Time: 6:36 PM
 */

namespace AppBundle\Controller\Users;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Route Prefix
 *
 * @Route("/user")
 */

class UsersController extends Controller
{

    /**
     * @Route("/users", name="user_home")
     */
    public function indexAction(Request $request)
    {
        return $this->render('AppBundle:users:register.html.twig', array(
            'page_header_title'     => $this->getParameter('user_page_header_title'),
            'page_header_subtitle'  => $this->getParameter('user_page_header_subtitle')
        ));
    }

    /**
     * @Route("/register", name="user_form")
     * @param Request $request
     * @return string
     */
    public function registerAction(Request $request)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $encoder = $this->get('security.encoder_factory')
                ->getEncoder($user);
            $password = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
            $user->setPassword($password);

            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->render('AppBundle:users:success_registered.html.twig', array(
                'name' => $user->getUsername()
            ));

        }

        return $this->render(
            'AppBundle:users:register.html.twig',
            array(
                'form'                  => $form->createView(),
                'page_header_title'     => $this->getParameter('user_page_header_title'),
                'page_header_subtitle'  => $this->getParameter('user_page_header_subtitle')
            )
        );
    }

}