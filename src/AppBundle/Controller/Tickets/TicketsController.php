<?php

namespace AppBundle\Controller\Tickets;

use AppBundle\Entity\Ticket;
use AppBundle\Entity\User;
use AppBundle\Form\TicketType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Route Prefix
 *
 * @Route("/")
 */

class TicketsController extends Controller
{

    /**
     * @Route("/open", name="ticket_open")
     */
    public function createAction(Request $request)
    {

        $ticket = new Ticket();

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(TicketType::class, $ticket);
        $form->handleRequest($request);

        if($form->isSubmitted() and $form->isValid()) {

            /*$em->persist($ticket);
            $em->flush();*/

            $this->sendTicketEmailAction($form['user']->getData());
             //return new Response("Ticket Create!");
        }

        $ticket = $em->getRepository("AppBundle:Ticket")->findAll();

        return $this->render(
            'AppBundle:ticket:create.html.twig',
            array(
                'form'                  => $form->createView(),
                'ticket'                => $ticket,
                'page_header_title'     => $this->getParameter('ticket_page_header_title'),
                'page_header_subtitle'  => $this->getParameter('ticket_page_header_subtitle')
            )
        );
    }

    /**
     * @param $user object of class User
     * @return Response
     */
    public function sendTicketEmailAction($user)
    {
        // Create Message
        $message = (new \Swift_Message('A ticket was assigned to you!'))
            ->setFrom('wandyhernandez86@gmail.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView('@App/users/success_registered.html.twig',
                    array('name' => $user->getUsername())
                ),
                'text/html'
            );

        // Send
        $this->get('mailer')->send($message);

        return new Response("Message Send");
    }
}
