<?php
/**
 * Created by PhpStorm.
 * User: Wandy
 * Date: 2018-03-07
 * Time: 10:23 PM
 */

namespace AppBundle\Controller\Roles;

use AppBundle\Entity\Role;
use AppBundle\Form\RoleType;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Route Prefix
 *
 * @Route("/roles")
 */
class RolesController extends Controller
{

    /**
     * @Route("/create", name="roles_create")
     */
    public function createAction(Request $request)
    {
        $role = new Role();
        $form = $this->createForm(RoleType::class, $role);
        $em = $this->getDoctrine()->getManager();

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {

            $em->persist($role);
            $em->flush($role);

            $this->redirectToRoute('roles_all');
        }

        return $this->render(
            'AppBundle:roles:create.html.twig',
            array(
                'form'    => $form->createView(),
                'roles'   => $em->getRepository("AppBundle:Role")->findAll(),
                'page_header_title'     => $this->getParameter('role_page_header_title'),
                'page_header_subtitle'  => $this->getParameter('role_page_header_subtitle')
            )
        );

    }

    /**
     * @Route("/assign", name="roles_assign")
     */
    public function assignRolesAction(Request $request)
    {
        
        return $this->render(
            'AppBundle:roles:assign.html.twig',
            array(
                'page_header_title'     => $this->getParameter('role_page_header_title'),
                'page_header_subtitle'  => $this->getParameter('assign_role_page_header_subtitle')
            )
        );

    }

    /**
     * @Route("/all", name="roles_all")
     */
    public function allAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $roles = $em->getRepository("AppBundle:Role")->findAll();

        $form = $this->createForm(RoleType::class, $roles);

        return $this->render(
            '@App/roles/create.html.twig',
            array(
                'form'   => $form->createView(),
                'roles'  => $roles,
                'page_header_title'     => $this->getParameter('role_page_header_title'),
                'page_header_subtitle'  => $this->getParameter('role_page_header_subtitle')
            )
        );
    }


}