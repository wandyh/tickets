<?php
/**
 * Created by PhpStorm.
 * User: Wandy
 * Date: 2018-03-07
 * Time: 6:46 PM
 */

namespace AppBundle\Controller\Companies;

use AppBundle\Entity\Company;
use AppBundle\Form\CompanyType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Route Prefix
 *
 * @Route("/company")
 */

class CompaniesController extends Controller
{

    /**
     * @Route("/create", name="company_create")
     */
    public function createAction(Request $request)
    {

        $comp = new Company();

        $form = $this->createForm(CompanyType::class, $comp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $em = $this->getDoctrine()->getManager();
            $em->persist($comp);
            $em->flush();

            //return new Response("Registered Company!");
            $this->redirectToRoute('company_all');
        }

        $em = $this->getDoctrine()->getManager();
        $comp = $em->getRepository("AppBundle:Company")->findAll();

        return $this->render(
            'AppBundle:companies:create.html.twig',
            array(
                'form'                  => $form->createView(),
                'companies'             => $comp,
                'page_header_title'     => $this->getParameter('company_page_header_title'),
                'page_header_subtitle'  => $this->getParameter('company_page_header_subtitle')
            )
        );
    }

    /**
     * @Route("/all", name="company_all")
     */
    public function allAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $comp = $em->getRepository("AppBundle:Company")->findAll();

        $form = $this->createForm(CompanyType::class, $comp);
        $form->handleRequest($request);

        return $this->render(
            'AppBundle:companies:create.html.twig',
            array(
                'form'                  => $form->createView(),
                'companies'             => $comp,
                'page_header_title'     => $this->getParameter('company_page_header_title'),
                'page_header_subtitle'  => $this->getParameter('company_page_header_subtitle')
            )
        );
    }

}